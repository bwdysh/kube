using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MassTransit;
using kube.contracts;
using System;

namespace kube.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            _ = services.AddMassTransit(x =>
              {
                  x.UsingRabbitMq((context, cfg) =>
                  {
                      cfg.Host(new Uri("rabbitmq://rabbitmq"), "/", rmq =>
                      {
                          rmq.Username("admin");
                          rmq.Password("admin");
                      });

                      cfg.ReceiveEndpoint("kube-api", ep => { });
                  });

                  x.AddRequestClient<PingRequest>();
              });

            services.AddMassTransitHostedService();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var rewriteOptions = new RewriteOptions()
                .AddRedirect("^$", "weatherforecast");
            app.UseRewriter(rewriteOptions);
        }
    }
}
