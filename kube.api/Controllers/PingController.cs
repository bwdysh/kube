using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using kube.contracts;
using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace kube.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PingController : ControllerBase
    {
        private readonly IRequestClient<PingRequest> _requestClient;

        public PingController(IRequestClient<PingRequest> requestClient)
        {
            _requestClient = requestClient;
        }

        // GET: api/Ping
        [HttpGet]
        public async Task<string> Get()
        {
            try
            {
                var request = new PingRequest { CreatedAt = DateTime.Now, Sender = Dns.GetHostName() };
                var response = await _requestClient.GetResponse<PingResponse>(request);

                return response.Message.Message;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
            
        }

        
    }
}
