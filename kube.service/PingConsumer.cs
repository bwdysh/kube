﻿using System;
using System.Net;
using System.Threading.Tasks;
using kube.contracts;
using MassTransit;

namespace kube.service
{
    public class PingConsumer : IConsumer<PingRequest>
    {
        public Task Consume(ConsumeContext<PingRequest> context)
        {
            var response = new PingResponse
            {
                Message = $"{Dns.GetHostName()} received ping of:{context.Message.CreatedAt} and responded at: {DateTime.Now} to {context.Message.Sender}."
            };

        
            return context.RespondAsync(response);
        }
    }
}