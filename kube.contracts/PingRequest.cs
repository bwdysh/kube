﻿using System;
using System.Net;

namespace kube.contracts
{
    public class PingRequest
    {
        public string Sender { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
