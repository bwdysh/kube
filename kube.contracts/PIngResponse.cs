﻿using System;
using System.Net;

namespace kube.contracts
{
    public class PingResponse
    {
        public string Message
        {
            get;
            set;
        }
    }   
}
